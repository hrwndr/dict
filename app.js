let loader = document.querySelector('.preloader-wrapper');
loader.style.display = 'none';
document.querySelector('.searchForm').addEventListener('submit', e => {
	e.preventDefault();
	loader.style.display = 'block';
	let term = document.querySelector('#term').value.toUpperCase();
	fetch('https://raw.githubusercontent.com/adambom/dictionary/master/dictionary.json')
	.then(file => file.json()
	).then(data => {
	  for(let x in data) {
	    if(x == term) {
	    	loader.style.display = 'none';
	    	document.querySelector('.output').innerHTML = `<span style="padding: 5%" class="col s12 m12 teal z-depth-3 hoverable left-align"><h5>${x} :</h5><p/><i>&nbsp;${data[x]}</i></span>`;
	    }
		// if(x !== term) {
		// 	loader.style.display = 'none';
  //   	document.querySelector('.output').innerHTML = `<span style="padding: 2%" class="col s12 m12 red z-depth-3 hoverable center-align">
		// <b>Error:</b> Word Not Found!</span>`;
		// return false;
		// }
		// return true;
	  }
	}).catch(err => console.log(err));
	});